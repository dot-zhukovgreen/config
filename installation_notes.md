# GENERAL

- sudo apt install gnome-tweaks   # setup select window on focus
- sudo apt install install htop
- sudo apt install ncdu

# GIT

- ln -sf /home/$USER/Dropbox/code/my_configs/.gitconfig /home/$USER/.gitconfig

# SSH

- rm -fr .ssh && ln -sf /home/$USER/Dropbox/code/my_configs/.ssh/ /home/$USER/
- sudo chmod 400 .ssh/id_rsa

# KITTY

- sudo apt install kitty
- ln -sf /home/$USER/Dropbox/code/my_configs/kitty.conf /home/$USER/.config/kitty/kitty.conf
- gsettings set org.gnome.desktop.default-applications.terminal exec 'kitty'
- sudo apt install fonts-firacode

# ZSH

- sudo apt install zsh
- sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"
- chsh -s $(which zsh) # reboot now
- ln -sf /home/$USER/Dropbox/code/my_configs/.zshrc /home/$USER/.zshrc
- ln -sf /home/$USER/Dropbox/code/my_configs/.zfunc /home/$USER/.zfunc
- git clone https://github.com/zsh-users/zsh-syntax-highlighting.git ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-syntax-highlighting
- git clone https://github.com/zsh-users/zsh-autosuggestions ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-autosuggestions

# VIM

- sudo apt install neovim
- mkdir ~/.config/nvim/ && ln -sf /home/$USER/Dropbox/code/my_configs/init.vim /home/$USER/.config/nvim/init.vim
- mkdir -p ~/.config/nvim/bundle
- git clone https://github.com/VundleVim/Vundle.vim.git ~/.config/nvim/bundle/Vundle.vim
- sudo apt install fzf ripgrep
- nvim +PluginInstall

# PYENV

- sudo apt install make build-essential libssl-dev zlib1g-dev libbz2-dev libreadline-dev libsqlite3-dev wget curl llvm libncurses5-dev xz-utils tk-dev libffi-dev liblzma-dev python-openssl
- curl -L https://github.com/pyenv/pyenv-installer/raw/master/bin/pyenv-installer | bash
- pyenv install 3.8-dev
- pyenv global 3.8-dev

# PYTHON

- pip install poetry
- poetry completions zsh > ~/.zfunc/_poetry
- ln -sf /home/$USER/Dropbox/code/my_configs/pyproject.toml /home/$USER/pyproject.toml
- ln -sf /home/$USER/Dropbox/code/my_configs/poetry.toml /home/$USER/poetry.toml
- cd ~ && poetry install


# Port frowarding for internet

- ssh -D 1080 -Nf mail

# DOCKER

- sudo apt install docker.io
- sudo groupadd docker
- sudo gpasswd -a $USER docker
- newgrp docker
ln -s /home/azhukov/Documents/.config/init.vim .ideavimrc
