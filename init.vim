filetype off

set autoread
set shell=zsh
set nocompatible            " Disable compatibility to old-time vim
set showmatch               " Show matching brackets.
set ignorecase              " Do case insensitive matching
set hlsearch                " highlight search results
set tabstop=4               " number of columns occupied by a tab character
set softtabstop=4           " see multiple spaces as tabstops so <BS> does the right thing
set expandtab               " converts tabs to white space
set shiftwidth=4            " width for autoindents
set autoindent              " indent a new line the same amount as the line just typed
set number                  " add line numbers
set mouse-=a
set ruler
set bs=2
set mousehide
set mouse=a
set modeline
set clipboard+=unnamed
set nowrap

"Tab stuff
set expandtab
set smarttab

let data_dir = has('nvim') ? stdpath('data') . '/site' : '~/.vim'
if empty(glob(data_dir . '/autoload/plug.vim'))
  silent execute '!curl -fLo '.data_dir.'/autoload/plug.vim --create-dirs  https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

call plug#begin('~/.config/nvim/plugged')

Plug 'scrooloose/nerdtree'
Plug 'ryanoasis/vim-devicons'
Plug 'tiagofumo/vim-nerdtree-syntax-highlight'
Plug 'scrooloose/nerdcommenter'
Plug 'vim-airline/vim-airline'
Plug 'numirias/semshi', {'do': ':UpdateRemotePlugins'}
Plug 'junegunn/fzf.vim'
Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
Plug 'sheerun/vim-polyglot'
Plug 'tmhedberg/SimpylFold'
Plug 'jiangmiao/auto-pairs'
Plug 'neoclide/coc.nvim', {'branch': 'release'}
Plug 'scalameta/coc-metals', {'do': 'yarn install --frozen-lockfile'}
Plug 'yaegassy/coc-ruff', {'do': 'yarn install --frozen-lockfile'}
Plug 'yaegassy/coc-mypy', {'do': 'yarn install --frozen-lockfile'}

call plug#end()

autocmd FileType python let b:coc_root_patterns = ['.git', '.env', 'venv', '.venv', 'setup.cfg', 'setup.py', 'pyproject.toml', 'pyrightconfig.json']

" GoTo code navigation.
nmap <silent> gd <Plug>(coc-definition)
nmap <silent> gy <Plug>(coc-type-definition)
nmap <silent> gi <Plug>(coc-implementation)
nmap <silent> gr <Plug>(coc-references)

" Apply AutoFix to problem on the current line.
nmap <leader>qf  <Plug>(coc-fix-current)

" Add autoformating shortcut
nmap <leader>f :call CocAction('format') <CR>
            \:call CocAction('runCommand', 'editor.action.organizeImport')<CR>

" Open CocCommand on dot

nmap , :CocCommand<CR>

"---------------------"
"AIRLINE SETUP"
"---------------------"
let g:airline_powerline_fonts = 1
if !exists('g:airline_symbols')
      let g:airline_symbols = {}
      endif
      let g:airline_symbols.space = "\ua0"
        let g:airline#extensions#tabline#enabled = 1
        let g:airline#extensions#tabline#show_buffers = 0

"------------------------"
"NERDTREE PLUGIN SETTINGS
"------------------------"

"Shortcut for NERDTreeToggle
map <leader>r :NERDTreeFind<cr>


"Show hidden files in NerdTree
let NERDTreeShowHidden=1
let NERDTreeIgnore = ['\.pyc$','__pycache__$']
map <leader>r :NERDTreeFind<cr>

"autopen NERDTree and focus cursor in new document
nmap <silent> <F2> :NERDTreeToggle<CR>
imap <silent> <F2> :NERDTreeToggle<CR>

"---------------------"
"FZF"
"---------------------"
nmap ; :Files<CR>
nmap ' :Rg<CR>

"costum bindings"
nmap <space> za

"neovim
let g:python3_host_prog = $PYENV_ROOT . '/shims/python3'
