# If you come from bash you might have to change your $PATH.
# export MANPATH="/usr/local/man:$MANPATH"

# Path to your oh-my-zsh installation.
export ZSH=/Users/$USER/.oh-my-zsh
export PATH=/usr/local/opt/curl/bin:/usr/local/sbin:$HOME/.local/bin:$PATH

# Set name of the theme to load. Optionally, if you set this to "random"
# it'll load a random theme each time that oh-my-zsh is loaded.
# See https://github.com/robbyrussell/oh-my-zsh/wiki/Themes
ZSH_THEME="frontcube"
DEFAULT_USER=$USER

# Uncomment the following line to use case-sensitive completion.
# CASE_SENSITIVE="true"

# Uncomment the following line to use hyphen-insensitive completion. Case
# sensitive completion must be off. _ and - will be interchangeable.
# HYPHEN_INSENSITIVE="true"

# Uncomment the following line to disable bi-weekly auto-update checks.
# DISABLE_AUTO_UPDATE="true"

# Uncomment the following line to change how often to auto-update (in days).
# export UPDATE_ZSH_DAYS=13

# Uncomment the following line to disable colors in ls.
# DISABLE_LS_COLORS="true"

# Uncomment the following line to disable auto-setting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment the following line to enable command auto-correction.
# ENABLE_CORRECTION="true"

# Uncomment the following line to display red dots whilst waiting for completion.
# COMPLETION_WAITING_DOTS="true"

# Uncomment the following line if you want to disable marking untracked files
# under VCS as dirty. This makes repository status check for large repositories
# much, much faster.
# DISABLE_UNTRACKED_FILES_DIRTY="true"

# Uncomment the following line if you want to change the command execution time
# stamp shown in the history command output.
# The optional three formats: "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
# HIST_STAMPS="mm/dd/yyyy"

# Would you like to use another custom folder than $ZSH/custom?
# ZSH_CUSTOM=/path/to/new-custom-folder

# Which plugins would you like to load? (plugins can be found in ~/.oh-my-zsh/plugins/*)
# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.

plugins=(
    git
    git-prompt
    docker
    docker-compose
    vagrant
    ansible
    python
    virtualenv 
    httpie
    colored-man-pages
    ssh-agent 
    gpg-agent
    zsh-syntax-highlighting
    zsh-autosuggestions
 pip fzf)

source $ZSH/oh-my-zsh.sh

# User configuration

# You may need to manually set your language environment
export LANG=en_US.UTF-8

export EDITOR='nvim'
# Preferred editor for local and remote sessions
# if [[ -n $SSH_CONNECTION ]]; then
#   export EDITOR='vim'
# else
#   export EDITOR='mvim'
# fi

# Compilation flags
# export ARCHFLAGS="-arch x86_64"

# ssh
# export SSH_KEY_PATH="~/.ssh/rsa_id"

# Set personal aliases, overriding those provided by oh-my-zsh libs,
# plugins, and themes. Aliases can be placed here, though oh-my-zsh
# users are encouraged to define aliases within the ZSH_CUSTOM folder.
# For a full list of active aliases, run `alias`.
#
# Vars used in aliases
# Example aliases
# alias zshconfig="mate ~/.zshrc"
# alias ohmyzsh="mate ~/.oh-my-zsh"
alias vim="nvim"
# alias ssh="kitty +kitten ssh"
alias icat="kitty +kitten icat"
alias update="
    brew update -v\
    && brew upgrade -v\
    && brew upgrade -v\
    && brew upgrade --cask --greedy\
    && brew autoremove -v\
    && pip install pip --upgrade \
    && pip install setuptools --upgrade \
    && pip install poetry --upgrade \
    && pip install pdm --upgrade \
    && cd ~ && pdm update --update-eager \
"
fpath+=~/.zfunc
fpath+=$(brew --prefix)/share/zsh/site-functions

autoload -Uz compinit && compinit

# pyenv
export PYENV_ROOT="$HOME/.pyenv"
export PATH="$PYENV_ROOT/shims:$PATH"
export LDFLAGS="-L/usr/local/opt/bzip2/lib"
export CPPFLAGS="-I/usr/local/opt/bzip2/include"
eval "$(pyenv init --path)"
eval "$(pyenv init -)"
# eval "$(pyenv virtualenv-init -)"
#
# kubernetes
# source <(kubectl completion zsh)
# source <(kompose completion zsh)
# source <(minikube completion zsh)
source <(register-python-argcomplete pytest)
eval "$(starship init zsh)"

[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh

# # nerdctl config
# alias nerdctl="nerdctl.lima"
# source <(nerdctl.lima completion zsh)
# eval "compdef _nerdctl nerdctl.lima"

# docker
export PATH=$PATH:$HOME/.docker/bin
export DOCKER_DEFAULT_PLATFORM=linux/amd64


# spark
export JAVA_HOME=/Library/Java/JavaVirtualMachines/zulu-17.jdk/Contents/Home

# export SPARK_HOME=$HOME/spark-3.4/spark-3.4.0-bin-hadoop3
# export PYSPARK_PYTHON=/Users/azhukov/.pyenv/shims/python3.11
# export PYSPARK_DRIVER_PYTHON=$PYSPARK_PYTHON
# 
# export PATH=$SPARK_HOME/bin:$PATH
# export PYTHONPATH=$SPARK_HOME/python/:$PYTHONPATH
# export PYTHONPATH=$SPARK_HOME/python/lib/*.zip:$PYTHONPATH

# export SPARK_HOME=$HOME/spark-3.3/spark-3.3.2-bin-hadoop3
# export PYSPARK_PYTHON=/Users/azhukov/.pyenv/shims/python3.10
# export PYSPARK_DRIVER_PYTHON=$PYSPARK_PYTHON


# >>> conda initialize >>>
# !! Contents within this block are managed by 'conda init' !!
__conda_setup="$('/opt/homebrew/Caskroom/miniconda/base/bin/conda' 'shell.zsh' 'hook' 2> /dev/null)"
if [ $? -eq 0 ]; then
    eval "$__conda_setup"
else
    if [ -f "/opt/homebrew/Caskroom/miniconda/base/etc/profile.d/conda.sh" ]; then
        . "/opt/homebrew/Caskroom/miniconda/base/etc/profile.d/conda.sh"
    else
        export PATH="/opt/homebrew/Caskroom/miniconda/base/bin:$PATH"
    fi
fi
unset __conda_setup
# <<< conda initialize <<<

# git-bug
alias bug="git-bug"


# Shell-GPT integration ZSH v0.2
alias sg="sgpt"
_sgpt_zsh() {
if [[ -n "$BUFFER" ]]; then
    _sgpt_prev_cmd=$BUFFER
    BUFFER+="⌛"
    zle -I && zle redisplay
    BUFFER=$(sgpt --shell <<< "$_sgpt_prev_cmd" --no-interaction)
    zle end-of-line
fi
}
zle -N _sgpt_zsh
bindkey '4;6u' _sgpt_zsh
# Shell-GPT integration ZSH v0.2
